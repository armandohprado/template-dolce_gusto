$(document).ready(function () {
    $('[rel="tooltip"]').tooltip();

    var $validator = $('.wizard-card form').validate({
        rules: {
            firstname: {
                required: true
            },
            telefone: {
                required: true
            },
            email: {
                required: true
            },
            cep: {
                required: true
            },
            'produto[]': {
                required: true,

            }
        },
    });

    $('.wizard-card').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',

        onNext: function (tab, navigation, index) {
            var $valid = $('.wizard-card form').valid();
            if (!$valid) {
                $validator.focusInvalid();
                return false;
            }
        },
        onInit: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            $width = 100 / $total;
            navigation.find('li').css('width', $width + '%');
        },
        onTabClick: function (tab, navigation, index) {
            var $valid = $('.wizard-card form').valid();
            if (!$valid) {
                return false;
            } else {
                return true;
            }
        },
        onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $wizard = navigation.closest('.wizard-card');

            if ($current >= $total - 1) {
                $($wizard).find('.btn-next').hide();
                $($wizard).find('.btn-finish').show();
            }
            else {
                $($wizard).find('.btn-next').show();
                $($wizard).find('.btn-finish').hide();
            }

            if ($current >= $total) {
                $($wizard).find('.btn-next').hide();
                $($wizard).find('.btn-finish').hide();
            }

            var move_distance = 100 / $total;
            move_distance = move_distance * (index) + move_distance / 2;

            $wizard.find($('.progress-bar')).css({ width: move_distance + '%' });
            $wizard.find($('.wizard-card .nav-pills li.active a .icon-circle')).addClass('checked');
        }
    });

    $('[data-toggle="wizard-checkbox"]').click(function () {
        wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-checkbox"]').removeClass('active');
        $('#maquina').text('Cafeteira ' + $(this).find('[type="checkbox"]').val());
        $(this).addClass('active');
        $(this).find('[type="checkbox"]').attr('checked', 'true');
    });

    $('.set-full-height').css('height', 'auto');
});



$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}