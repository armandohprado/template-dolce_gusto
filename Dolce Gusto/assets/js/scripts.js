$(document).ready(function () {
    var serviceCep = {
        onComplete: function (value) {
            var cep = value.replace(/\D/g, '');
            var script = document.createElement('script');
            script.src = 'https://viacep.com.br/ws/' + value + '/json/?callback=cepCallBack';
            document.body.appendChild(script);
        },
    };

    Typed.new('.spn-change', {
        strings: ["Me contrata pooorfavor! :)", "A melhor e a mais pratica do mercado.", "Vamos tomar um café?", "Que dia eu posso começar?"],
        typeSpeed: 9,
        startDelay: 2,
        backSpeed: 9,
        backDelay: 500,
        loop: true
    });

    $('.phone').mask('(00) 0000-0000');
    $('.cep').mask('00000-000', serviceCep);
});

function cepCallBack(content) {
    if (!('erro' in content)) {
        $('input[name="logradouro"').val(content.logradouro).change();
        viewModel.logradouro = content.logradouro;

        $('input[name="localidade"').val(content.localidade + " / " + content.uf).change();
        viewModel.localidade = content.localidade + "-" + content.uf;

        $('input[name="complemento"]').attr('disabled', false);
        $('input[name="complemento"]').focus();
    }
    else {
        $('input[name="logradouro"').val('Endereço não encontrado').change();
        viewModel.logradouro = '';

        $('input[name="localidade"').val('').change();
        viewModel.localidade = '';

        $('input[name="complemento"]').attr('disabled', true);
        $('input[name="complemento"]').val('').change();
        $('input[name="cep"]').val('').change();
        $('input[name="cep"]').focus();
    }
}