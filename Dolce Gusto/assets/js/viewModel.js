var viewModel = function () {
    self = this;

    self.email = ko.observable('');
    self.nome = ko.observable('');
    self.telefone = ko.observable('');
    self.cep = ko.observable('');
    self.logradouro = ko.observable('');
    self.complemento = ko.observable('');
    self.localidade = ko.observable('');

    self.confirmCep = ko.computed(function () {
        return self.cep();
    }, self);

    self.confirmEstado = ko.computed(function () {
        return self.localidade();
    }, self);

    self.confirmNome = ko.computed(function () {
        return self.nome();
    }, self);
    self.confirmEmail = ko.computed(function () {
        return self.email();
    }, self);

    self.confirmTel = ko.computed(function () {
        return self.telefone();
    }, self);

    self.confirmEndereco = ko.computed(function () {
        return self.logradouro() + ' ' + self.complemento();
    }, self);

    self.submitForm = function () {
        var jsonData = {
            nome: self.nome(),
            telefone: self.telefone(),
            email: self.email(),
            produto: self.produto(),
            endereco: self.confirmEndereco(),
            cep: self.confirmCep(),
            localidade: self.localidade()
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/echo/json/',
            data: { json: JSON.stringify(jsonData) },
            success: function (data) {
                var wizard = $('.wizard-card').bootstrapWizard();
                wizard.bootstrapWizard('next');
            },
            error: function () {
                var wizard = $('.wizard-card').bootstrapWizard();
                wizard.bootstrapWizard('next');
            }
        });
    };
};

ko.applyBindings(new viewModel());